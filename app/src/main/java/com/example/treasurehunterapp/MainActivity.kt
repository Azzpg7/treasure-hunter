package com.example.treasurehunterapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.treasurehunterapp.data.Datasource
import com.example.treasurehunterapp.model.*
import com.example.treasurehunterapp.ui.theme.TreasureHunterAppTheme

// Main in android app
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TreasureHunterApp()
        }
    }
}

// Our main app with Scaffold that we fill with list
@Composable
fun TreasureHunterApp() {
    TreasureHunterAppTheme {
        val navController = rememberNavController()
        val billList: SnapshotStateList<Bill> = Datasource().loadBills()
        val transactionList: SnapshotStateList<Transaction> = Datasource().loadTransactinos()

        NavHost(navController = navController, startDestination = "home") {
            composable("home") {
                DefaultActivity(billList, transactionList, navController)
            }
            composable("transactions") {
                AddTransactionActivity(billList, transactionList, navController)
            }
        }
    }
}

@Composable
fun DefaultActivity(
    billList: SnapshotStateList<Bill>,
    transactionList: SnapshotStateList<Transaction>,
    navController: NavController,
) {
    val bottomMenuItemsList = prepareBottomMenu()
    val selectedItem = remember { mutableStateOf("Bills") }
    val openDialog = remember { mutableStateOf(false) }

    @Composable
    fun AddButton() {
        FloatingActionButton(onClick = {
            openDialog.value = true
        }) {
            Icon(
                imageVector = Icons.Default.Add, contentDescription = "Add new bill"
            )
        }
    }

    @Composable
    fun BottomMenu() {
        BottomNavigation {
            bottomMenuItemsList.forEach { menuItem ->
                BottomNavigationItem(
                    selected = (selectedItem.value == menuItem.label),
                    onClick = { selectedItem.value = menuItem.label },
                    icon = {
                        Icon(
                            imageVector = menuItem.icon,
                            contentDescription = menuItem.label
                        )
                    },
                    label = { Text(text = menuItem.label) },
                    enabled = true
                )
            }
        }
    }

    Scaffold(floatingActionButton = { AddButton() },
        bottomBar = { BottomMenu() },
        content = {
            if (selectedItem.value == "Bills") {
                BillList(billList)
                if (openDialog.value) {
                    AddBillDialog(openDialog = openDialog, billList = billList)
                }
            } else if (selectedItem.value == "Transactions") {
                TransactionList(transactionList)
                if (openDialog.value) {
                    openDialog.value = false
                    navController.navigate("transactions")
                }
            }
        })
}

@Composable
fun AddTransactionActivity(
    billList: SnapshotStateList<Bill>,
    transactionList: SnapshotStateList<Transaction>,
    navController: NavController
) {
    val fromBill = rememberSaveable { mutableStateOf("bill1") }
    val toBill = rememberSaveable { mutableStateOf("bill2") }
    val moneyAmount = rememberSaveable { mutableStateOf("0") }
    val openDialogTo = remember { mutableStateOf(false) }
    val openDialogFrom = remember { mutableStateOf(false) }

    fun processTransaction(from: String, to: String, amount: Int) {
        billList.map {
            if (it.name == from) {
                it.value -= amount
            } else if (it.name == to) {
                it.value += amount
            }
        }
    }

    @Composable
    fun confirmButton() {
        FloatingActionButton(onClick = {
            processTransaction(fromBill.value, toBill.value, moneyAmount.value.toInt())
            transactionList.add(
                Transaction(
                    fromBill.value, toBill.value, moneyAmount.value.toInt()
                )
            )
            navController.navigate("home")
        }) {
            Icon(imageVector = Icons.Default.Done, contentDescription = "Ok")
        }
    }

    @Composable
    fun moneyAmountField() {
        TextField(
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            value = moneyAmount.value,
            onValueChange = { moneyAmount.value = it },
            label = {
                Text(text = "скока")
            },
            modifier = Modifier.fillMaxWidth()
        )
    }

    @Composable
    fun fromBillDialog() {
        AlertDialog(onDismissRequest = { openDialogFrom.value = false }, buttons = {
            LazyColumn() {
                items(billList) { bill ->
                    Button(onClick = {
                        fromBill.value = bill.name
                        openDialogFrom.value = false
                    }, content = { Text(text = bill.name) })
                }
            }
        })
    }

    @Composable
    fun toBillDialog() {
        AlertDialog(onDismissRequest = { openDialogFrom.value = false }, buttons = {
            LazyColumn() {
                items(billList) { bill ->
                    Button(onClick = {
                        toBill.value = bill.name
                        openDialogTo.value = false
                    }, content = { Text(text = bill.name) })
                }
            }
        })
    }

    Scaffold(floatingActionButton = { confirmButton() }, content = {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            moneyAmountField()
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .sizeIn(minHeight = 120.dp)
            ) {
                Column(Modifier.weight(0.5f)) {
                    Text(text = "From")
                    Text(
                        text = fromBill.value,
                        modifier = Modifier.clickable { openDialogFrom.value = true })
                }
                Column(Modifier.weight(0.5f)) {
                    Text(text = "To")
                    Text(text = toBill.value,
                        modifier = Modifier.clickable { openDialogTo.value = true })
                }
            }

            if (openDialogFrom.value) {
                fromBillDialog()
            }

            if (openDialogTo.value) {
                toBillDialog()
            }
        }
    })
}

@Composable
fun AddBillDialog(openDialog: MutableState<Boolean>, billList: SnapshotStateList<Bill>) {
    val billnameState = rememberSaveable { mutableStateOf("") }
    val amountmoneyState = rememberSaveable { mutableStateOf("") }
    AlertDialog(onDismissRequest = {
        // Dismiss the dialog when the user clicks outside the dialog or on the back
        // button. If you want to disable that functionality, simply use an empty
        // onCloseRequest.
        openDialog.value = false
    }, title = {
        Text(text = "Add bill")
    }, text = {
        Column(
            verticalArrangement = Arrangement.Center
        ) {
            TextField(
                value = billnameState.value,
                onValueChange = { billnameState.value = it },
                label = {
                    Text(text = "Bill name")
                },
                modifier = Modifier
                    .padding(top = 16.dp)
                    .sizeIn(minHeight = 120.dp)
                    .background(Color.Transparent)
            )
            TextField(
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
                value = amountmoneyState.value,
                onValueChange = { amountmoneyState.value = it },
                label = {
                    Text(text = "Money amount")
                },
                modifier = Modifier
                    .padding(top = 16.dp)
                    .sizeIn(minHeight = 120.dp)
                    .background(Color.Transparent)
            )
        }
    }, confirmButton = {
        Button(onClick = {
            billList.add(Bill(billnameState.value, amountmoneyState.value.toInt()))
            openDialog.value = false
        }) {
            Text("Add")
        }
    }, dismissButton = {
        Button(onClick = {
            openDialog.value = false
        }) {
            Text("Cancel")
        }
    })
}
