package com.example.treasurehunterapp.data

import android.annotation.SuppressLint
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import com.example.treasurehunterapp.model.Bill
import com.example.treasurehunterapp.model.Transaction

// class that load our bills into list.
class Datasource() {
    @SuppressLint("ResourceType")
    fun loadBills(): SnapshotStateList<Bill> {
        val Bills: SnapshotStateList<Bill> = mutableStateListOf<Bill>(
            Bill("Bebebe", 1000),
            Bill("Tinkoff", 1000),
        )
        return Bills
    }

    @SuppressLint("ResourceType")
    fun loadTransactinos(): SnapshotStateList<Transaction> {
        val Transactions: SnapshotStateList<Transaction> = mutableStateListOf<Transaction>(
            Transaction("bebebebankir", "Sber", 1000),
            Transaction("Bebebanka", "Alpha", 1000),
        )
        return Transactions
    }
}