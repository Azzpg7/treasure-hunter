package com.example.treasurehunterapp.model

// Our Bill class with name on String and money value on it
data class Bill (
    val name: String, var value: Int
)
