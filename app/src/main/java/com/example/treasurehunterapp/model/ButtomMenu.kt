package com.example.treasurehunterapp.model

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector

data class BottomMenuItem(
    val label: String, val icon: ImageVector
)

fun prepareBottomMenu(): List<BottomMenuItem> {
    val bottomMenuItemsList = arrayListOf<BottomMenuItem>()

    bottomMenuItemsList.add(BottomMenuItem(label = "Bills", icon = Icons.Filled.Home))
    bottomMenuItemsList.add(BottomMenuItem(label = "Transactions", icon = Icons.Filled.Person))

    return bottomMenuItemsList
}
