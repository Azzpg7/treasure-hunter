package com.example.treasurehunterapp.model

data class Transaction(
    val from: String, val to: String, val moneyAmount: Int
)
