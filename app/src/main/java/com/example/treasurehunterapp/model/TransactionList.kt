package com.example.treasurehunterapp.model

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp


@Composable
fun TransactionCard(transaction: Transaction, modifier: Modifier = Modifier) {
    Card(modifier = modifier.padding(8.dp), elevation = 4.dp) {
        Column() {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(5.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = transaction.moneyAmount.toString() + " ₽",
                    modifier = Modifier.padding(16.dp),
                    style = MaterialTheme.typography.h6
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(5.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = transaction.from,
                    modifier = Modifier.padding(16.dp),
                    style = MaterialTheme.typography.h6
                )
                Text(
                    text = transaction.to,
                    modifier = Modifier.padding(16.dp),
                    style = MaterialTheme.typography.h6
                )
            }
        }

    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TransactionList(transactionList: List<Transaction>, modifier: Modifier = Modifier) {
    LazyColumn() {
        items(transactionList) { transaction ->
            TransactionCard(
                transaction, modifier = Modifier.animateItemPlacement()
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ButtonList(
    billList: SnapshotStateList<Bill>, choice: MutableState<String>, modifier: Modifier = Modifier
) {
    LazyColumn() {
        items(billList) { bill ->
            Button(onClick = { choice.value = bill.name }, content = { Text(text = bill.name) })
        }
    }
}